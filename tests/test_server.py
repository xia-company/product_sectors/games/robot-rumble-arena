import pytest
from robot_rumble_arena.server import Server

def test_connect():
    server = Server()
    player = server.connect("Player 1", "password")
    assert player is not None
    assert player.name == "Player 1"
    assert player.password == "password"
    assert server.is_player_connected(player.id) == True

def test_disconnect():
    server = Server()
    player = server.connect("Player 1", "password")
    server.disconnect(player)
    assert server.is_player_connected(player.id) == False

def test_start_battle():
    server = Server()
    player1 = server.connect("Player 1", "password")
    player2 = server.connect("Player 2", "password")
    server.start_battle(player1, player2)
    assert len(server.battles) == 1
    assert server.battles[0]["player1"] == player1
    assert server.battles[0]["player2"] == player2

def test_update_battles():
    server = Server()
    player1 = server.connect("Player 1", "password")
    player2 = server.connect("Player 2", "password")
    server.start_battle(player1, player2)
    server.update_battles()
    assert len(server.battles) == 0