class RobotComponent:
    def __init__(self, id: int, name: str, power: float, speed: float):
        self.id = id
        self.name = name
        self.power = power
        self.speed = speed