import pytest
from robot_rumble_arena.actions import Actions

def test_move_robot():
    actions = Actions()
    success = actions.move_robot(1, "up")
    assert success == True

def test_attack_robot():
    actions = Actions()
    success = actions.attack_robot(1, 2)
    assert success == True

def test_use_ability():
    actions = Actions()
    success = actions.use_ability(1, "shield")
    assert success == True