import pytest
from robot_rumble_arena.database import Database

def test_create_player_data():
    db = Database()
    player_data = db.create_player_data("Player 1", "password")
    assert player_data is not None
    assert player_data["name"] == "Player 1"
    assert player_data["password"] == "password"

def test_get_player_data():
    db = Database()
    player_data = db.create_player_data("Player 1", "password")
    retrieved_data = db.get_player_data(player_data["id"])
    assert retrieved_data is not None
    assert retrieved_data["name"] == "Player 1"
    assert retrieved_data["password"] == "password"

def test_update_player_data():
    db = Database()
    player_data = db.create_player_data("Player 1", "password")
    updated_data = {"name": "Updated Player 1", "password": "updated_password"}
    db.update_player_data(player_data["id"], updated_data)
    retrieved_data = db.get_player_data(player_data["id"])
    assert retrieved_data is not None
    assert retrieved_data["name"] == "Updated Player 1"
    assert retrieved_data["password"] == "updated_password"