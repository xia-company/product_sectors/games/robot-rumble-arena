import pytest
from robot_rumble_arena.game import Game, Robot

def test_add_robot():
    game = Game()
    robot = Robot(1, "Robot 1", 10, 5)
    game.add_robot(robot)
    assert len(game.robots) == 1
    assert game.robots[0] == robot

def test_remove_robot():
    game = Game()
    robot = Robot(1, "Robot 1", 10, 5)
    game.add_robot(robot)
    game.remove_robot(robot)
    assert len(game.robots) == 0

def test_start_battle():
    game = Game()
    robot1 = Robot(1, "Robot 1", 10, 5)
    robot2 = Robot(2, "Robot 2", 8, 6)
    game.add_robot(robot1)
    game.add_robot(robot2)

    game.start_battle(1, 2)

    assert game.is_battle_in_progress() == True

    while game.is_battle_in_progress():
        game.update_battle()

    assert game.is_battle_in_progress() == False
    assert game.get_battle_winner() is not None

def test_update_game_state():
    game = Game()
    robot = Robot(1, "Robot 1", 10, 5)
    game.add_robot(robot)

    game.update_game_state()

    assert game.get_game_state() is not None
    assert len(game.get_game_state()) == 1
    assert game.get_game_state()[0]["name"] == "Robot 1"