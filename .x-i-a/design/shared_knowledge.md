```python
[
    "'game.py' contains the Game class that manages the game state, robots, and battles.",
    "'robot.py' contains the Robot and RobotComponent classes that define the structure and behavior of robots and their components.",
    "'player.py' contains the Player class that represents a player in the game.",
    "'actions.py' contains the Actions class that handles the actions performed by players.",
    "'battle.py' contains the Battle class that manages battles between robots.",
    "'database.py' contains the Database class that handles database operations for storing game data."
]
```