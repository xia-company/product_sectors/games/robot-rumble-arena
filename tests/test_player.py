import pytest
from robot_rumble_arena.player import Player

def test_create_player():
    player = Player(1, "Player 1", "password")
    assert player.id == 1
    assert player.name == "Player 1"
    assert player.password == "password"