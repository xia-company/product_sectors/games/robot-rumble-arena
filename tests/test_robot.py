import pytest
from robot_rumble_arena.robot import Robot, RobotComponent

def test_add_component():
    robot = Robot(1, "Robot 1", 10, 5)
    component = RobotComponent(1, "Component 1", 5, 3)
    robot.add_component(component)
    assert len(robot.components) == 1
    assert robot.components[0] == component

def test_remove_component():
    robot = Robot(1, "Robot 1", 10, 5)
    component = RobotComponent(1, "Component 1", 5, 3)
    robot.add_component(component)
    robot.remove_component(component)
    assert len(robot.components) == 0

def test_calculate_total_power():
    robot = Robot(1, "Robot 1", 10, 5)
    component1 = RobotComponent(1, "Component 1", 5, 3)
    component2 = RobotComponent(2, "Component 2", 3, 4)
    robot.add_component(component1)
    robot.add_component(component2)
    assert robot.calculate_total_power() == 18

def test_calculate_total_speed():
    robot = Robot(1, "Robot 1", 10, 5)
    component1 = RobotComponent(1, "Component 1", 5, 3)
    component2 = RobotComponent(2, "Component 2", 3, 4)
    robot.add_component(component1)
    robot.add_component(component2)
    assert robot.calculate_total_speed() == 9