from .player import Player


class Actions:
    def __init__(self, player: Player):
        self.player = player

    def perform_action(self, action: str):
        # Perform the specified action