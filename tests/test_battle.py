import pytest
from robot_rumble_arena.battle import Battle

def test_start_battle():
    battle = Battle(1, 2)
    assert battle.is_in_progress() == True

def test_update_battle():
    battle = Battle(1, 2)
    battle.update()
    assert battle.is_in_progress() == False

def test_get_battle_result():
    battle = Battle(1, 2)
    battle.update()
    result = battle.get_result()
    assert result is not None
    assert result["winner"] is not None