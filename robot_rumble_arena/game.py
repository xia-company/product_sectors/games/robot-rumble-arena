from typing import List
from .robot import Robot


class Game:
    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name
        self.robots = []

    def add_robot(self, robot: Robot):
        self.robots.append(robot)

    def remove_robot(self, robot: Robot):
        self.robots.remove(robot)

    def start_battle(self, robot1_id: int, robot2_id: int):
        robot1 = self._find_robot(robot1_id)
        robot2 = self._find_robot(robot2_id)
        if robot1 and robot2:
            # Start the battle
            pass

    def _find_robot(self, robot_id: int) -> Robot:
        for robot in self.robots:
            if robot.id == robot_id:
                return robot
        return None