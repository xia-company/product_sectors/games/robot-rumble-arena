from typing import List
from .component import RobotComponent


class RobotComponent:
    def __init__(self, id: int, name: str, power: float, speed: float):
        self.id = id
        self.name = name
        self.power = power
        self.speed = speed


class Robot:
    def __init__(self, id: int, name: str, power: float, speed: float):
        self.id = id
        self.name = name
        self.power = power
        self.speed = speed
        self.components = []

    def add_component(self, component: RobotComponent):
        self.components.append(component)

    def remove_component(self, component: RobotComponent):
        self.components.remove(component)