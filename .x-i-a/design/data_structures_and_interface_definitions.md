```mermaid
classDiagram
    class RobotComponent{
        -int id
        -str name
        -float power
        -float speed
    }

    class Robot{
        -int id
        -str name
        -float power
        -float speed
        -List[RobotComponent] components
        +add_component(component: RobotComponent) -> None
        +remove_component(component: RobotComponent) -> None
    }

    class Game{
        -int id
        -str name
        -List[Robot] robots
        +add_robot(robot: Robot) -> None
        +remove_robot(robot: Robot) -> None
        +start_battle(robot1_id: int, robot2_id: int) -> None
    }
```