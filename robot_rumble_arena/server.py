class Server:
    def __init__(self, ip_address: str, port: int):
        self.ip_address = ip_address
        self.port = port
        self.connected_players = []

    def accept_connection(self, player: Player):
        self.connected_players.append(player)
        # Handle the connection logic

    def close_connection(self, player: Player):
        self.connected_players.remove(player)
        # Handle the disconnection logic

    def send_data(self, player: Player, data: dict):
        # Send data to the specified player

    def receive_data(self, player: Player) -> dict:
        # Receive data from the specified player