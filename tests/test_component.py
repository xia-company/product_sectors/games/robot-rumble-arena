import pytest
from robot_rumble_arena.robot import RobotComponent

def test_create_component():
    component = RobotComponent(1, "Component 1", 5, 3)
    assert component.id == 1
    assert component.name == "Component 1"
    assert component.power == 5
    assert component.speed == 3