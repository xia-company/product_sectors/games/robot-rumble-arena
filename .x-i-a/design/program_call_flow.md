```mermaid
sequenceDiagram
    participant P as Player
    participant S as Server
    participant G as Game

    P->>G: Customize robot
    G->>P: Robot customization options
    P->>G: Selected components
    G->>P: Robot updated

    P->>S: Connect to multiplayer server
    S->>P: Connection established

    loop Game Loop
        P->>S: Request game state update
        S->>P: Game state data

        P->>S: Perform action (e.g., move robot)
        S->>P: Action processed

        P->>S: Request battle
        S->>P: Battle initiated

        P->>S: Receive battle updates
        S->>P: Battle data

        P->>S: End battle
        S->>P: Battle results
    end
```